package com.noreilly.service;

import com.noreilly.grapher.model.LineChart;
import com.noreilly.grapher.model.LineChartDataPoint;
import com.noreilly.grapher.model.LineChartDataPointRepository;
import com.noreilly.grapher.model.LineChartRepository;
import org.omg.CORBA.TRANSACTION_REQUIRED;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

@Service
public class ChartService {

    @Autowired
    LineChartRepository lineChartRepository;
    @Autowired
    LineChartDataPointRepository lineChartDataPointRepository;

    Logger logger = LoggerFactory.getLogger(ChartService.class);

    public LineChart getLineChart(String name){
        LineChart lineChart = lineChartRepository.findOne(name);

        if(lineChart == null){
            logger.info("Did not find chart, creating one");
            lineChart = new LineChart();
            lineChart.setKey(name);
            lineChart.setValues( new ArrayList<LineChartDataPoint>());
        }
        Random random = new Random();
        LineChartDataPoint point = LineChartDataPoint.build()
                                        .setxAxisValue(String.valueOf(System.currentTimeMillis()))
                                        .setyAxisValue(String.valueOf(random.nextInt(100)))
                                        .createLineChartDataPoint();
       // lineChartDataPointRepository.save(point);
        logger.info("Nuber poins before save" + lineChart.getValues().size());
        lineChart.getValues().add(point);

        lineChartRepository.save(lineChart);

        logger.info("Nuber poins after save" + lineChart.getValues().size());

        return lineChart;
    }
}
