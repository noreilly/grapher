package com.noreilly.grapher.model;

import org.springframework.data.repository.CrudRepository;

public interface LineChartRepository extends CrudRepository<LineChart, String> {


}