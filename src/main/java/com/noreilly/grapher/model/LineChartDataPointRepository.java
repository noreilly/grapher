package com.noreilly.grapher.model;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface LineChartDataPointRepository extends CrudRepository<LineChartDataPoint, Long> {


}