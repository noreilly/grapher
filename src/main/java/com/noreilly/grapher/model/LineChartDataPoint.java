package com.noreilly.grapher.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;

@Entity
@Table(name = "line_chart_data_point")
public class LineChartDataPoint {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String xAxisValue;
	
	private String yAxisValue;

    public LineChartDataPoint(){

    }

    public LineChartDataPoint(String xAxisValue, String yAxisValue){
        this.setxAxisValue(xAxisValue);
        this.setyAxisValue(yAxisValue);

    }
    public Object getxAxisValue() {
        return xAxisValue;
    }

    public Object getyAxisValue() {
        return yAxisValue;
    }

    public static LineChartDataPointBuilder  build(){
        return new LineChartDataPointBuilder();
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setxAxisValue(final String xAxisValue) {
        this.xAxisValue = xAxisValue;
    }

    public void setyAxisValue(final String yAxisValue) {
        this.yAxisValue = yAxisValue;
    }

    public static class LineChartDataPointBuilder {
        private String xAxisValue;
        private String yAxisValue;

        public LineChartDataPointBuilder setxAxisValue(final String xAxisValue) {
            this.xAxisValue = xAxisValue;
            return this;
        }

        public LineChartDataPointBuilder setyAxisValue(final String yAxisValue) {
            this.yAxisValue = yAxisValue;
            return this;
        }

        public LineChartDataPoint createLineChartDataPoint() {
            return new LineChartDataPoint(xAxisValue, yAxisValue);
        }
    }

}
