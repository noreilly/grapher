package com.noreilly.grapher.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "line_chart")
public class LineChart {

    @Id
    private String key;

    @OneToMany(cascade = CascadeType.ALL)
    private List<LineChartDataPoint> values;

    private String xAxisLabel;
    private String yAxisLabel;

    public LineChart(){

    }
    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public List<LineChartDataPoint> getValues() {
        return values;
    }

    public void setValues(final List<LineChartDataPoint> values) {
        this.values = values;
    }

    public String getxAxisLabel() {
        return xAxisLabel;
    }

    public void setxAxisLabel(final String xAxisLabel) {
        this.xAxisLabel = xAxisLabel;
    }

    public String getyAxisLabel() {
        return yAxisLabel;
    }

    public void setyAxisLabel(final String yAxisLabel) {
        this.yAxisLabel = yAxisLabel;
    }
}
