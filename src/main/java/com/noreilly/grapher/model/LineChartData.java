package com.noreilly.grapher.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LineChartData {

	String key;
	List<Object[]> values;

    public LineChartData(){
        values = new ArrayList<Object[]>();
    }
    public LineChartData(LineChart lineChart){

        values = new ArrayList<Object[]>();

        for (LineChartDataPoint dataPoint : lineChart.getValues()) {
            Object[] entry = new Object[] {
                    dataPoint.getxAxisValue(),
                    dataPoint.getyAxisValue() };
            values.add(entry);

        }
        this.key = lineChart.getKey();
        this.xAxisLabel = lineChart.getxAxisLabel();
        this.yAxisLabel = lineChart.getyAxisLabel();
    }

	String xAxisLabel;
	String yAxisLabel;
	
	public String getKey() {
		return key;
	}
	public List<Object[]> getValues() {
		return values;
	}
	public String getxAxisLabel() {
		return xAxisLabel;
	}
	public String getyAxisLabel() {
		return yAxisLabel;
	}

    public List<LineChartData> asList(){
        return Arrays.asList(this);
    }

    public void addDataPoint(LineChartDataPoint dataPoint){
        Object[] entry = new Object[] {
                dataPoint.getxAxisValue(),
                dataPoint.getyAxisValue() };
        values.add(entry);

    }
}
