package com.noreilly.rest;

import com.noreilly.grapher.model.*;
import com.noreilly.service.ChartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/stats")
public class ReadingResource {

    Logger logger = LoggerFactory.getLogger(ReadingResource.class);

    @Autowired
    ChartService chartService;


    @RequestMapping(value="/{name}/graph", method = RequestMethod.GET)
    @ResponseBody
    @Transactional
	public List<LineChartData> getAllDataOnly(@PathVariable("name") String name) {

        logger.info("Recieved request for " + name);
        LineChart lineChart = chartService.getLineChart(name);

            return new LineChartData(lineChart).asList();

    }



}