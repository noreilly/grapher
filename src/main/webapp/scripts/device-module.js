'use strict';

angular.module('DeviceModule', [])
	
	.factory('DeviceResource', function($resource, $routeParams, resourceBaseUrl) {
		
		return $resource(resourceBaseUrl + '/devices/:deviceId', {deviceId:'@deviceId'});	
		 
	});
