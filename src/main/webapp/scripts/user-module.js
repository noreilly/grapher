'use strict';

angular.module('UserModule', ['ngResource'])
	.factory('UserResource', function($resource, resourceBaseUrl) {
		
		return $resource(resourceBaseUrl + '/users', {}, {
			retrieveAll: {method:'GET', isArray:true, transformResponse: function(data, headers) {

				return JSON.parse(data).content;
			}},
			
			retrieve: {method:'GET', params:{userId:'userId'}, url: resourceBaseUrl + '/users/:userId', isArray:false},
			
			findByUsername: {method:'GET', params:{username:'username'}, url: resourceBaseUrl + '/users/search/findByUsername?username:=username', isArray:false, transformResponse: function(data, headers) {
				return JSON.parse(data).content[0];
			}}
		});
	});