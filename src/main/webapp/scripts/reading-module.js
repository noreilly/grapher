'use strict';

angular.module('ReadingModule', [])
	
	.factory('ReadingResource', function($resource, $routeParams, resourceBaseUrl) {
		
//		return $resource('http://localhost:8080\:8080/data-logger/api/devices/:deviceId/readings', 
//		
//		'getSelect': { method: 'GET', isArray: true, params: { deviceId: '@deviceId' } }});	
//		
		
//		return  $resource('http://localhost:8080\:8080/data-logger/api/devices/:deviceId/readings',{deviceId: '@deviceId'}, {
//			  'get': { method: 'GET', isArray: true}
//			});
		return $resource(resourceBaseUrl + '/devices/:deviceId/readings', {deviceId:'@deviceId'});	
	});
