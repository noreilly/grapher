'use strict';

cirrusWorkbench.controller('DevicesController', [ '$scope', '$location',
		'DeviceResource', function($scope, $location, ProjectResource) {

			$scope.devices = ProjectResource.query();

			
			$scope.edit = function(deviceMacAddress) {
				
				$location.path("/devices/" + deviceMacAddress);
				
			};
			
			
		} ]);