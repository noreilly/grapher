'use strict';

cirrusWorkbench.controller('ProjectController', ['$scope', '$location', '$routeParams', '$resource', 'DeviceResource', 'resourceBaseUrl', function($scope, $location, $routeParams, $resource, DeviceResource, resourceBaseUrl) {

	$scope.hasId= function(){return false;};

	if ($routeParams.projectId) {

		$scope.project = DeviceResource.get({projectId: $routeParams.projectId}); 

		$scope.isUpdate= true;
		$scope.header = "Update Project";

	} else {

		$scope.isUpdate= false;
		$scope.header = "Create Project";
	}

	var updateResource = $resource(resourceBaseUrl + '/projects/' + $routeParams.projectId,
			{projectId:'@id'}, {
				update: {method:'PUT', url: resourceBaseUrl + '/projects/' + $routeParams.projectId, isArray:false}
			});

	$scope.remove = function() {

		DeviceResource.remove({projectId: $routeParams.projectId},
				function(data) {
			$location.path('/projects');
		},
		function(error) {
			alert('error');
			alert(error);
		});
	};

	$scope.save = function() {
		if($routeParams.projectId) {
			updateResource.update($scope.project,
					function(data) {
				$location.path('/projects');
			},
			function(error) {
				console.log('error');
				console.log(error);
			});
		}else{
				DeviceResource.save($scope.project,
				function(data) {
					$location.path('/projects');
				},
				function(error) {
					alert('error');
					alert(error);
				});
			};
		};
		$scope.cancel = function() {
			$location.path("/projects/");
		};
	
	}]);