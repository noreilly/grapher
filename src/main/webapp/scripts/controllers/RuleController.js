'use strict';

cirrusWorkbench.controller('RuleController', ['$scope', '$location', '$routeParams', '$resource', 'RuleResource', 'resourceBaseUrl', function($scope, $location, $routeParams, $resource, RuleResource, resourceBaseUrl) {

	 
	$scope.save = function() {
	
				RuleResource.save({deviceId: $routeParams.deviceId}, $scope.rule,
				function(data) {
					$location.path('/devices/' + $routeParams.deviceId);
				},
				function(error) {
					alert('error');
					alert(error);
				});

		};
	
	}]);