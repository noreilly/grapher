'use strict';

cirrusWorkbench.controller('ReadingsController', [ '$scope', '$location','$routeParams',
		'ReadingResource', function($scope, $location, $routeParams, ReadingResource) {

			$scope.readings = ReadingResource.query({deviceId: $routeParams.deviceId});
			
		} ]);