'use strict';

cirrusWorkbench.controller('DeviceController', ['$scope', '$location', '$routeParams', '$resource', 'DeviceResource', 'resourceBaseUrl', function($scope, $location, $routeParams, $resource, DeviceResource, resourceBaseUrl) {

	if( $routeParams.deviceId != null){
		$scope.device = DeviceResource.get({deviceId: $routeParams.deviceId});
	}
	 
	$scope.save = function() {
	
				DeviceResource.save($scope.device,
				function(data) {
					$location.path('/devices');
				},
				function(error) {
					alert('error');
					alert(error);
				});

		};
	
	}]);