'use strict';

cirrusWorkbench.controller('SidebarCtrl', ['$scope', '$location', function($scope, $location) {

	$scope.isActive = function(location) {
	
		return $location.path() == location;
	};
	
}]);
