'use strict';

angular.module('ChartModule', [])
	
	.factory('ChartResource', function($resource, $routeParams, resourceBaseUrl) {
		
		return $resource(resourceBaseUrl + '/stats/:dataName/graph');
				
	});
