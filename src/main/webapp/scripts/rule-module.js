'use strict';

angular.module('RuleModule', [])
	
	.factory('RuleResource', function($resource, $routeParams, resourceBaseUrl) {
		
		return $resource(resourceBaseUrl + '/devices/:deviceId/rules', {deviceId:'@deviceId'});	
		 
	});
