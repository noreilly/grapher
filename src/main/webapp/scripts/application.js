'use strict';

if(window.location.port != ""){
	var resourceBaseUrl =  "http://" + window.location.hostname + ":" + window.location.port + "\:" + window.location.port + "/grapher/api";
} else {
	var resourceBaseUrl = "http://" + window.location.hostname + "/grapher/api";
}


var cirrusWorkbench = angular.module('cirrus-workbench', ['DeviceModule','RuleModule','ReadingModule', 'UserModule','ChartModule', 'nvd3ChartDirectives'])
	.config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
		$routeProvider
			.when('/graph', {
				templateUrl: 'views/readingsGraph.html'
				
			})
			.when('/dashboard', {
                templateUrl: 'views/dashboard.html'

            })
            .when('/new', {
                            templateUrl: 'views/newDashboard.html'

                        })

			   .when('/newStyle', {
                                        templateUrl: 'views/newStyle.html'

                                    })
			.otherwise({
				redirectTo: '/dashboard'
			});
		
	

	
	}])
	
	
	.constant("resourceBaseUrl",resourceBaseUrl)

	.controller('SidebarCtrl', ['$scope', '$location', function($scope, $location) {

		$scope.isActive = function(location) {
		
			return $location.path() == location;
		};
		
	}]);
